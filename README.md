k apply -k .

kubectl exec -n spire spire-server-0 -- \
    /opt/spire/bin/spire-server entry create \
    -node  \
    -spiffeID spiffe://example.org/ns/spire/sa/spire-agent \
    -selector k8s_sat:cluster:demo-cluster \
    -selector k8s_sat:agent_ns:spire \
    -selector k8s_sat:agent_sa:spire-agent

kubectl exec -n spire spire-server-0 -- \
    /opt/spire/bin/spire-server entry create \
    -parentID spiffe://example.org/ns/spire/sa/spire-agent \
    -spiffeID spiffe://example.org/ns/default/sa/default/postgres \
    -selector k8s:ns:default \
    -selector k8s:sa:default \
    -selector k8s:pod-label:app:postgres \
    -selector k8s:container-name:spiffe-helper

k apply -f deploy/postgres/.

kubectl exec $(k get pod -l app=postgres -o name) -c postgres -- \
    su postgres -c "psql -f /etc/postgresql/create_user.sql"

kubectl exec -n spire spire-server-0 -- \
    /opt/spire/bin/spire-server entry create \
    -parentID spiffe://example.org/ns/spire/sa/spire-agent \
    -spiffeID spiffe://example.org/ns/default/sa/default/myapp \
    -selector k8s:ns:default \
    -selector k8s:sa:default \
    -selector k8s:pod-label:app:myapp \
    -selector k8s:container-name:spiffe-helper \
    -dns myuser

k apply -f deploy/client/.

kubectl exec $(kubectl get pod -l app=myapp -o name) -c myapp -- \
    psql "port=5432 host=postgres-svc.default.svc.cluster.local user=myuser dbname=newdb sslcert=/opt/spire/certs/postgresql/svid.pem sslkey=/opt/spire/certs/postgresql/svid.key sslrootcert=/opt/spire/certs/postgresql/svid_bundle.pem sslmode=verify-ca" -f /etc/postgresql/insert_data.sql

kubectl exec $(kubectl get pod -l app=myapp -o name) -c myapp -- \
    psql "port=5432 host=postgres-svc.default.svc.cluster.local user=myuser dbname=newdb sslcert=/opt/spire/certs/postgresql/svid.pem sslkey=/opt/spire/certs/postgresql/svid.key sslrootcert=/opt/spire/certs/postgresql/svid_bundle.pem sslmode=verify-ca" -c "SELECT * FROM employees;"

kubectl exec $(kubectl get pod -l app=myapp -o name) -c myapp -- \
    psql "port=5432 host=postgres-svc.default.svc.cluster.local user=myuser dbname=newdb sslcert=/opt/spire/certs/postgresql/svid.pem sslkey=/opt/spire/certs/postgresql/svid.key sslrootcert=/opt/spire/certs/postgresql/svid_bundle.pem sslmode=verify-ca" -c "SELECT department, AVG(salary) AS average_salary FROM employees GROUP BY department;"